from kivy.uix.dropdown import DropDown
from kivy.uix.button import Button

class DropDownMenu(Button):
    def __init__(self, menu_items, select_cb, **kwargs):
        super(DropDownMenu, self).__init__(**kwargs)
        self.dropdown = None
        self.dropdown = DropDown()

        for item in menu_items + ["None"]:
            btn = Button(text=item, size_hint_y=None, height=50)
            btn.bind(on_release=lambda btn: self.dropdown.select(btn.text))

            self.dropdown.add_widget(btn)

        self.bind(on_release=self.dropdown.open)
        self.dropdown.bind(on_select=lambda instance, x: select_cb(x))