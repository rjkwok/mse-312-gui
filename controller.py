import serial
import threading
from collections import deque

# project imports
from csvlogger import CsvLogger

class Controller:

    STATE_TABLE = ["IDLE", "MOVETO_PICKUP", "MOVETO_DROP", "MOVETO_INITIAL"]

    def __init__(self):
        # serial UART port used to communicate with MCU
        self.serial_port = None

        # logging object for file management abstraction
        self.log = CsvLogger("mse-312-log")
        self.logging_enabled = True

        # threading infrastructure for fast non-blocking serial comms and logging
        self.update_thread = None
        self.thread_stop_event = threading.Event()
        self.cmd_deque = deque([]) # thread-safe FIFO for buffering commands to send over UART

        # Controller position determined from encoder, tracked for display only
        self.pos = 0

        # Controller PID gains, tracked for display only
        self.kp = 0
        self.ki = 0
        self.kd = 0
        
        # Controller firmware state, tracked for display only
        self.state = 0

        # Controller setpoint, tracked for display only
        self.setpoint = 0

        # Controller PID output, tracked for display only
        self.pid_output = 0

        # Controller tolerances for accuracy and precision used to determine when the setpoint has been reached
        self.error_tolerance = 0
        self.ripple_tolerance = 0

        # Controller timer value, tracked for display only
        self.timer_value = 0

    def __del__(self):
        self.disconnect_serial_port()

    def disconnect_serial_port(self):
        if self.serial_port:
            self.thread_stop_event.set()
            self.update_thread.join()
            self.update_thread = None
            self.serial_port.close()
            self.serial_port = None

    def open_serial_port(self, line, baud=115200):
        self.disconnect_serial_port()
        try:
            self.serial_port = serial.Serial(port=line, baudrate=baud)
            self.update_thread = threading.Thread(None, self.update)
            self.update_thread.start()
            return True
        except:
            self.serial_port = None
            self.update_thread = None
            return False

    def validate_serial(self):
        return not self.serial_port == None        

    def get_logging_enabled(self):
        return self.logging_enabled

    def toggle_logging(self, *args):
        self.logging_enabled = not self.logging_enabled

    # blocks waiting for a serial data packet to read
    def update(self):
        while not self.thread_stop_event.is_set():
            if not self.validate_serial():
                raise RuntimeError("Controller serial connection does not exist")

            try:
                if len(self.cmd_deque) > 0:
                    self.serial_port.write((self.cmd_deque.popleft() + "\n").encode())
                string_values = self.serial_port.readline().decode().rstrip(" \n\r\t").split(",")
                
                # these assignments must match the send order in the FW
                self.pos = int(string_values[0])
                self.kp = float(string_values[1])
                self.ki = float(string_values[2])
                self.kd = float(string_values[3])
                self.state = int(string_values[4])
                self.setpoint = int(string_values[5])
                self.pid_output = float(string_values[6])
                self.error_tolerance = int(string_values[7])
                self.ripple_tolerance = int(string_values[8])
                self.timer_value = int(string_values[9])

            # catch any errors in parsing without crashing program
            except Exception as e:
                print(e)

            if self.logging_enabled:
                self.log.writerow([
                    self.get_pos(), self.get_kp(), self.get_ki(), self.get_kd(),
                    self.get_state(), self.get_setpoint(), self.get_pid_output(), 
                    self.get_error_tolerance(), self.get_ripple_tolerance()])

    # buffers command in FIFO. Serial validation and writing occurs in update thread.
    def send_command(self, cmd):
        self.cmd_deque.append(cmd)

    def get_pos(self):
        return self.pos

    def zero(self, *args):
        self.send_command("z")

    def get_kp(self):
        return self.kp

    def increment_kp(self, *args):  
        self.send_command("p")
        self.send_command("k")

    def decrement_kp(self, *args):  
        self.send_command("p")
        self.send_command("j")

    def get_ki(self):
        return self.ki

    def increment_ki(self, *args):  
        self.send_command("i")
        self.send_command("k")

    def decrement_ki(self, *args):  
        self.send_command("i")
        self.send_command("j")

    def get_kd(self):
        return self.kd

    def increment_kd(self, *args):  
        self.send_command("d")
        self.send_command("k")
    
    def decrement_kd(self, *args):      
        self.send_command("d")
        self.send_command("j")

    def get_state(self):
        return self.STATE_TABLE[self.state]

    def stop(self, *args):
        self.send_command("s")

    def trigger(self, *args):
        self.send_command("g")

    def get_setpoint(self):
        return self.setpoint

    def get_pid_output(self):
        return self.pid_output

    def get_error_tolerance(self):
        return self.error_tolerance

    def increment_error_tolerance(self, *args):
        self.send_command("x")
        self.send_command("k")

    def decrement_error_tolerance(self, *args):
        self.send_command("x")
        self.send_command("j")

    def get_ripple_tolerance(self):
        return self.ripple_tolerance

    def increment_ripple_tolerance(self, *args):
        self.send_command("r")
        self.send_command("k")

    def decrement_ripple_tolerance(self, *args):
        self.send_command("r")
        self.send_command("j")
    
    def get_timer_value(self):
        return self.timer_value