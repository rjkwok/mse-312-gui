from kivy.uix.widget import Widget
from kivy.graphics import Color
from kivy_garden.graph import Graph, LinePlot
from collections import deque

class RealtimeGraph(Graph):

    X_RANGE = 1000

    def __init__(self, **kwargs):
        super(RealtimeGraph, self).__init__(**kwargs)
        self.samples1 = deque([])
        self.samples2 = deque([])
        self.plot1 = LinePlot(line_width=2, color=[128/255.0, 165/255.0, 41/255.0, 1])
        self.plot2 = LinePlot(line_width=2, color=[53/255.0, 157/255.0, 203/255.0, 1])
        self.add_plot(self.plot1)
        self.add_plot(self.plot2)
        self.xmin = 0
        self.xmax = self.X_RANGE

    def update(self, y1, y2):
        if len(self.samples1) == self.X_RANGE:
            self.samples1.popleft()
            self.samples2.popleft()
        self.samples1.append(y1)
        self.samples2.append(y2)
        self.plot1.points = [(i, self.samples1[i]) for i in range(len(self.samples1))]
        self.plot2.points = [(i, self.samples2[i]) for i in range(len(self.samples2))]