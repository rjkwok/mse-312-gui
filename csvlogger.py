import os
import time
import datetime
import csv

# returns an unused file path constructed from a specfied prefix, suffix, and the current datetime
def generate_unique_file_path(prefix, suffix, output_directory):
    index = 0
    while(1):
        file_name = '{0}-{1}-{2}.{3}'.format(prefix, datetime.datetime.now().strftime("%d-%H-%M"), index, suffix)
        file_path = output_directory + file_name
        if not os.path.isfile(file_path):
            return file_path
        index = index + 1

# manages csv log file and timestamps each written line
class CsvLogger:

	def __init__(self, prefix, output_directory="./"):
		self.file_path = generate_unique_file_path(prefix, "csv", output_directory)
		self.file = open(self.file_path, "w")
		self.writer = csv.writer(self.file, delimiter=',', quoting=csv.QUOTE_NONE)

	def get_file_path(self):
		return self.file_path

	def writerow(self, values):
		self.writer.writerow([time.time()] + values)

	def __del__(self):
		self.file.close()
