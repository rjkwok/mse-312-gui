from kivy.app import App
from kivy.uix.anchorlayout import AnchorLayout
from kivy.uix.gridlayout import GridLayout
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.clock import Clock
from kivy_garden.graph import Graph, MeshLinePlot
from kivy.graphics import Color
import sys
import glob
import serial

# project files
from controller import Controller
from dropdownmenu import DropDownMenu
from realtimegraph import RealtimeGraph

# states
IDLE = 0
CONNECTED = 1

def get_padded_layout(widget, size_hint):
    padded_layout = AnchorLayout()
    widget.size_hint = size_hint
    padded_layout.add_widget(widget)
    return padded_layout

class Readout(Label):
    def __init__(self, **kwargs):
        super(Readout, self).__init__(**kwargs)

class RoundedButton(Button):
    def __init__(self, **kwargs):
        super(RoundedButton, self).__init__(**kwargs)

class RootLayout(BoxLayout):
    def __init__(self, **kwargs):
        super(RootLayout, self).__init__(**kwargs)

class MSE312App(App):

    def __init__(self, **kwargs):
        super(MSE312App, self).__init__(**kwargs)
        # MCU interface abstraction
        self.controller = Controller()
        # Application state. Currently no loop behaviour difference between IDLE and CONNECTED
        self.state = IDLE

    # Returns list of the serial ports available on the system
    def get_serial_ports(self):
        if sys.platform.startswith('win'):
            ports = ['COM%s' % (i + 1) for i in range(256)]
        elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
            # this excludes your current terminal "/dev/tty"
            ports = glob.glob('/dev/tty[A-Za-z]*')
        elif sys.platform.startswith('darwin'):
            ports = glob.glob('/dev/tty.*')
        else:
            raise EnvironmentError('Unsupported platform')

        result = []
        for port in ports:
            try:
                s = serial.Serial(port)
                s.close()
                result.append(port)
            except (OSError, serial.SerialException):
                pass
        return result

    def set_serial_port(self, line):
        self.controller.open_serial_port(line)
        if self.controller.validate_serial():
            self.serial_menu.text = "Serial: " + line
            self.transition(CONNECTED)
        else:
            self.serial_menu.text = "Serial: None"
            self.transition(IDLE)

    def update_widget_values(self, *args):
        self.pos_label.text = "{:d}".format(self.controller.get_pos())
        self.kp_label.text = "{:.4f}".format(self.controller.get_kp())
        self.ki_label.text = "{:.4f}".format(self.controller.get_ki())
        self.kd_label.text = "{:.4f}".format(self.controller.get_kd())
        self.state_label.text = self.controller.get_state()
        self.setpoint_label.text = "{:d}".format(self.controller.get_setpoint())
        self.pid_output_label.text = "{:.4f}".format(self.controller.get_pid_output())
        self.pos_graph.update(self.controller.get_pos(), self.controller.get_setpoint())
        self.error_label.text = "{:d}".format(self.controller.get_error_tolerance())
        self.ripple_label.text = "{:d}".format(self.controller.get_ripple_tolerance())
        self.logging_label.text = "ON" if self.controller.get_logging_enabled() else "OFF"
        self.timer_label.text = "{:d}".format(self.controller.get_timer_value())

    # state transition safety checks and state setup
    # TODO: grey-out labels when not updating
    def transition(self, state):
        if state == IDLE:
            self.zero_button.disabled = True
            self.kp_increment_button.disabled = True
            self.kp_decrement_button.disabled = True
            self.ki_increment_button.disabled = True
            self.ki_decrement_button.disabled = True
            self.kd_increment_button.disabled = True
            self.kd_decrement_button.disabled = True
            self.error_increment_button.disabled = True
            self.error_decrement_button.disabled = True
            self.ripple_increment_button.disabled = True
            self.ripple_decrement_button.disabled = True
            self.stop_button.disabled = True
            self.trigger_button.disabled = True
            self.state = IDLE

        elif state == CONNECTED:
            self.zero_button.disabled = False
            self.kp_increment_button.disabled = False
            self.kp_decrement_button.disabled = False
            self.ki_increment_button.disabled = False
            self.ki_decrement_button.disabled = False
            self.kd_increment_button.disabled = False
            self.kd_decrement_button.disabled = False
            self.error_increment_button.disabled = False
            self.error_decrement_button.disabled = False
            self.ripple_increment_button.disabled = False
            self.ripple_decrement_button.disabled = False
            self.stop_button.disabled = False
            self.trigger_button.disabled = False
            self.state = CONNECTED

        else:
            raise ValueError("{0} is not a valid state.".format(state))

    def build(self):
        root = RootLayout(orientation="horizontal")
        
        self.pos_graph = RealtimeGraph(xlabel="time(s)", ylabel="position(degrees)", y_grid_label=True, padding=5, 
            x_grid=True, y_grid=True, xmin=0, ymin=-360, ymax=360, x_ticks_major=100, y_ticks_major=90, size_hint=(0.5, 1))
        #self.pos_graph.background_color = [0.9, 0.9, 0.9, 1]
        root.add_widget(self.pos_graph)

        # Readouts and buttons arranged in a vertical section
        sidebar_layout = BoxLayout(orientation="vertical", padding=5, size_hint=(0.5, 1))

        # Menu for managing the serial connection
        self.serial_menu = DropDownMenu(self.get_serial_ports(), self.set_serial_port, text='Serial: None', size_hint=(1.0, None), height=44)
        sidebar_layout.add_widget(self.serial_menu)

        control_panel_layout = GridLayout(cols=4, size_hint=(1.0, 0.5))
        
        # Logging status display
        control_panel_layout.add_widget(Label(text="[b]Logging: [/b]", markup=True))
        self.logging_label = Readout(text="ON")
        control_panel_layout.add_widget(get_padded_layout(self.logging_label, (0.95, 0.85)))
        self.logging_button = RoundedButton(text='[b]TOGGLE[/b]', markup=True)
        self.logging_button.font_size = 0.2*self.logging_button.height
        self.logging_button.bind(on_press=self.controller.toggle_logging)
        control_panel_layout.add_widget(get_padded_layout(self.logging_button, (0.95,0.85)))
        control_panel_layout.add_widget(Label(text=""))

        # Controller state display with buttons to stop or trigger execution
        control_panel_layout.add_widget(Label(text="[b]State: [/b]", markup=True))
        self.state_label = Readout(text="0")
        control_panel_layout.add_widget(get_padded_layout(self.state_label, (0.95, 0.85)))
        self.stop_button = RoundedButton(text='[b]STOP[/b]', markup=True)
        self.stop_button.font_size = 0.2*self.stop_button.height
        self.stop_button.bind(on_press=self.controller.stop)
        control_panel_layout.add_widget(get_padded_layout(self.stop_button, (0.95,0.85)))
        self.trigger_button = RoundedButton(text='[b]TRIGGER[/b]', markup=True)
        self.trigger_button.font_size = 0.2*self.trigger_button.height
        self.trigger_button.bind(on_press=self.controller.trigger)
        control_panel_layout.add_widget(get_padded_layout(self.trigger_button, (0.95,0.85)))

        # Position display
        control_panel_layout.add_widget(Label(text="[b]Position: [/b]", markup=True))
        self.pos_label = Readout(text="0")
        control_panel_layout.add_widget(get_padded_layout(self.pos_label, (0.95, 0.85)))
        self.zero_button = RoundedButton(text='[b]ZERO[/b]', markup=True)
        self.zero_button.font_size = 0.2*self.zero_button.height
        self.zero_button.bind(on_press=self.controller.zero)
        control_panel_layout.add_widget(get_padded_layout(self.zero_button, (0.95,0.85)))
        control_panel_layout.add_widget(Label(text=""))

        # Timer display
        control_panel_layout.add_widget(Label(text="[b]Timer (ms): [/b]", markup=True))
        self.timer_label = Readout(text="0")
        control_panel_layout.add_widget(get_padded_layout(self.timer_label, (0.95, 0.85)))
        control_panel_layout.add_widget(Label(text=""))
        control_panel_layout.add_widget(Label(text=""))

        # Setpoint display
        control_panel_layout.add_widget(Label(text="[b]Setpoint: [/b]", markup=True))
        self.setpoint_label = Readout(text="0")
        control_panel_layout.add_widget(get_padded_layout(self.setpoint_label, (0.95, 0.85)))
        control_panel_layout.add_widget(Label(text=""))
        control_panel_layout.add_widget(Label(text=""))

        # PID Output display
        control_panel_layout.add_widget(Label(text="[b]PID Output: [/b]", markup=True))
        self.pid_output_label = Readout(text="0")
        control_panel_layout.add_widget(get_padded_layout(self.pid_output_label, (0.95, 0.85)))
        control_panel_layout.add_widget(Label(text=""))
        control_panel_layout.add_widget(Label(text=""))

        # Kp display with decrement/increment buttons
        control_panel_layout.add_widget(Label(text="[b]Kp: [/b]", markup=True))
        self.kp_label = Readout(text="0.00")
        control_panel_layout.add_widget(get_padded_layout(self.kp_label, (0.95, 0.85)))
        self.kp_decrement_button = RoundedButton(text='[b]-[/b]', markup=True)
        self.kp_decrement_button.font_size = 0.5*self.kp_decrement_button.height
        self.kp_decrement_button.bind(on_press=self.controller.decrement_kp)
        control_panel_layout.add_widget(get_padded_layout(self.kp_decrement_button, (0.95,0.85)))
        self.kp_increment_button = RoundedButton(text='[b]+[/b]', markup=True)
        self.kp_increment_button.font_size = 0.5*self.kp_increment_button.height
        self.kp_increment_button.bind(on_press=self.controller.increment_kp)
        control_panel_layout.add_widget(get_padded_layout(self.kp_increment_button, (0.95,0.85)))

        # Ki display with decrement/increment buttons
        control_panel_layout.add_widget(Label(text="[b]Ki: [/b]", markup=True))
        self.ki_label = Readout(text="0.00")
        control_panel_layout.add_widget(get_padded_layout(self.ki_label, (0.95, 0.85)))
        self.ki_decrement_button = RoundedButton(text='[b]-[/b]', markup=True)
        self.ki_decrement_button.font_size = 0.5*self.ki_decrement_button.height
        self.ki_decrement_button.bind(on_press=self.controller.decrement_ki)
        control_panel_layout.add_widget(get_padded_layout(self.ki_decrement_button, (0.95,0.85)))
        self.ki_increment_button = RoundedButton(text='[b]+[/b]', markup=True)
        self.ki_increment_button.font_size = 0.5*self.ki_increment_button.height
        self.ki_increment_button.bind(on_press=self.controller.increment_ki)
        control_panel_layout.add_widget(get_padded_layout(self.ki_increment_button, (0.95,0.85)))

        # Kd display with decrement/increment buttons
        control_panel_layout.add_widget(Label(text="[b]Kd: [/b]", markup=True))
        self.kd_label = Readout(text="0.00")
        control_panel_layout.add_widget(get_padded_layout(self.kd_label, (0.95, 0.85)))
        self.kd_decrement_button = RoundedButton(text='[b]-[/b]', markup=True)
        self.kd_decrement_button.font_size = 0.5*self.kd_decrement_button.height
        self.kd_decrement_button.bind(on_press=self.controller.decrement_kd)
        control_panel_layout.add_widget(get_padded_layout(self.kd_decrement_button, (0.95,0.85)))
        self.kd_increment_button = RoundedButton(text='[b]+[/b]', markup=True)
        self.kd_increment_button.font_size = 0.5*self.kd_increment_button.height
        self.kd_increment_button.bind(on_press=self.controller.increment_kd)
        control_panel_layout.add_widget(get_padded_layout(self.kd_increment_button, (0.95,0.85)))

        # Error tolerance display with decrement/increment buttons
        control_panel_layout.add_widget(Label(text="[b]Error Tol: [/b]", markup=True))
        self.error_label = Readout(text="0.00")
        control_panel_layout.add_widget(get_padded_layout(self.error_label, (0.95, 0.85)))
        self.error_decrement_button = RoundedButton(text='[b]-[/b]', markup=True)
        self.error_decrement_button.font_size = 0.5*self.error_decrement_button.height
        self.error_decrement_button.bind(on_press=self.controller.decrement_error_tolerance)
        control_panel_layout.add_widget(get_padded_layout(self.error_decrement_button, (0.95,0.85)))
        self.error_increment_button = RoundedButton(text='[b]+[/b]', markup=True)
        self.error_increment_button.font_size = 0.5*self.error_increment_button.height
        self.error_increment_button.bind(on_press=self.controller.increment_error_tolerance)
        control_panel_layout.add_widget(get_padded_layout(self.error_increment_button, (0.95,0.85)))

        # Ripple tolerance display with decrement/increment buttons
        control_panel_layout.add_widget(Label(text="[b]Ripple Tol: [/b]", markup=True))
        self.ripple_label = Readout(text="0.00")
        control_panel_layout.add_widget(get_padded_layout(self.ripple_label, (0.95, 0.85)))
        self.ripple_decrement_button = RoundedButton(text='[b]-[/b]', markup=True)
        self.ripple_decrement_button.font_size = 0.5*self.ripple_decrement_button.height
        self.ripple_decrement_button.bind(on_press=self.controller.decrement_ripple_tolerance)
        control_panel_layout.add_widget(get_padded_layout(self.ripple_decrement_button, (0.95,0.85)))
        self.ripple_increment_button = RoundedButton(text='[b]+[/b]', markup=True)
        self.ripple_increment_button.font_size = 0.5*self.ripple_increment_button.height
        self.ripple_increment_button.bind(on_press=self.controller.increment_ripple_tolerance)
        control_panel_layout.add_widget(get_padded_layout(self.ripple_increment_button, (0.95,0.85)))

        sidebar_layout.add_widget(control_panel_layout)

        root.add_widget(sidebar_layout)

        # Adjust all widgets for starting conditions
        self.update_widget_values()

        # Set initial state
        self.transition(IDLE)

        # Schedule widgets to update at 100 Hz
        Clock.schedule_interval(self.update_widget_values, 0.01)

        return root

if __name__ == "__main__":

    MSE312App().run()