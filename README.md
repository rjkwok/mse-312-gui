SETUP:

Install Python 3.7 and the latest version of pip. Run the following commands to collect dependencies:

python -m pip install docutils pygments pypiwin32 kivy_deps.sdl2==0.1.22 kivy_deps.glew==0.1.12
python -m pip install kivy==1.11.1
python -m pip install https://github.com/kivy-garden/graph/archive/master.zip
python -m pip install pyserial

USE:

python mse-312-gui.py